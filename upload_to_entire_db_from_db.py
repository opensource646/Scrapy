import os
import json
import time
import requests
import datetime
import random
import csv
import string
import decimal
import uuid
# from selenium import webdriver
import pymongo
from pymongo import MongoClient
from bson.objectid import ObjectId
from multiprocessing.dummy import Pool as ThreadPool
from tqdm import tqdm


def actually_move(data):
    # temp_id = data["website_id"]
    # temp_id_string = str(temp_id).replace("ObjectId(\"","").replace("\")","")
    # data["website_id"] =
    id_temp = str(data["_id"]).replace("ObjectId(\"",'').replace("\")",'')
    name_object = list(name_collection.find({'_id':id_temp}))
    # print(name_object)
    name_description_object = list(name_description_collection.find({'_id':id_temp}))
    # print(name_description_object)
    try:
        data['name_vector'] = name_object[0]['name_vector']
    except:
        print('name vector not found')
        data['name_vector'] = [0]*768
    try:
        data['name_desc_vector'] = name_description_object[0]['name_vector']
    except:
        print('name desc vector not found')
        data['name_desc_vector'] = [0]*768

    # print(data)



    return data


if __name__ == '__main__':
    WEBSITE_ID_HASH = {}
    # WEBSITE_ID_HASH["queens_cz"] = "5bf39a8bc9a7f60004dd8d04"
    # WEBSITE_ID_HASH["zoot_cz"] = "5bf39a6fc9a7f60004dd8d03"
    # WEBSITE_ID_HASH["footshop_cz"] = "5bf39aa9c9a7f60004dd8d05"
    # WEBSITE_ID_HASH["answear_cz"] = "5bf399f7c9a7f60004dd8d01"
    # WEBSITE_ID_HASH["aboutyou_cz"] = "5bf399d3c9a7f60004dd8d00"
    # WEBSITE_ID_HASH["zalando_cz"] = "5bf39a55c9a7f60004dd8d02"
    # WEBSITE_ID_HASH["freshlabels_cz"] = "5c38cbea0359f800041cdab0"

    WEBSITE_ID_HASH["hervis_at"] = "5ba20a59c423de434b232c36"
    WEBSITE_ID_HASH["xxlsports_at"] = "5ba20a93c423de434b232c37"
    WEBSITE_ID_HASH["decathlon_at"] = "5ba20abdc423de434b232c38"
    WEBSITE_ID_HASH["gigasport_at"] = "5ba20ae2c423de434b232c39"
    WEBSITE_ID_HASH["bergzeit_at"] = "5ba20b02c423de434b232c3a"
    WEBSITE_ID_HASH["blue_tomato_at"] = "5ba20b2cc423de434b232c3b"
    # WEBSITE_ID_HASH["adidas_us"] = "5c7fdad66c251b000443b910"
    # WEBSITE_ID_HASH["puma_us"] = "5c7fda916c251b000443b90e"
    # WEBSITE_ID_HASH["asos_gb"] = "5bc055046264490004432328"
    # WEBSITE_ID_HASH["debenhams_gb"] = "5c3cae7c29ecc300044c5f67"
    # WEBSITE_ID_HASH["marksandspencer_gb"] = "5c3caf2429ecc300044c5f69"
    # ranknet_model_load = ranknet()
    # ranknet_model_load.load_weights("model/weights-improvement-05-0.26.h5")
    website_id_hash = WEBSITE_ID_HASH
    # posts = []
    # new_posts = []

    MONGODB_URL1 = 'mongodb://admin:epxNOGMHaAiRRV5q@mongodb-prod.greendeck.co:27017/admin'
    client_prod = pymongo.MongoClient(
        MONGODB_URL1,
        ssl=False
    )
    MONGODB_URL2 = 'mongodb://root:MTyvE6ikos87@mongodb-dev.greendeck.co:27017/admin'
    client_dev = pymongo.MongoClient(
        MONGODB_URL2,
        ssl=False
    )
    db_prod = client_prod.octopus_fashion
    db_dev = client_dev.faissal_dev
    collection = db_prod.combined_products
    new_collection = db_dev.dumps
    for key, website_id in website_id_hash.items():
        print(key)
        cc = collection.find({'website_id':ObjectId(website_id)})
        for post in tqdm(list(cc)):
            new_collection.insert_one(post)
