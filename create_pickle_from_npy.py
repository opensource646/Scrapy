import pandas as pd
import numpy as np
from tqdm import tqdm
import pickle
data = np.load('vector/resnet_vector_dict_jaccard_255k.npy').item()
final_list = []
for key,val in tqdm(data.items()):
    temp_dict = {}
    temp_dict['_id'] = str(key.split('/')[1][0:-4])
    temp_dict['vector'] = list(val)
    final_list.append(temp_dict)
print('pickle creating')
with open('pickle_vector/resnet_255k.pickle', 'wb') as f:
    pickle.dump(final_list, f)
print('pickle created')
