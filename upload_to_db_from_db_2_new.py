import os
import json
import time
import requests
import datetime
import random
import csv
import string
import decimal
import uuid
# from selenium import webdriver
import pymongo
from pymongo import MongoClient
from bson.objectid import ObjectId
from multiprocessing.dummy import Pool as ThreadPool
from tqdm import tqdm
from get_name_vector import load_name_bert_model
from get_name_desc_vector import load_name_description_bert_model

def actually_move(data):
    id_temp = data['_id']
    website_id = data['website_id']
    data['website_id'] = ObjectId(str(website_id))
    # data['website_id'] = ObjectId(temp_website_id)
    # id_temp = str(data['_id']).replace("ObjectId(\"",'').replace("\")",'')
    data_1 = list(source_collection_1.find({"_id":id_temp}))
    data_2 = list(source_collection_2.find({"_id":id_temp}))
    data_3 = list(source_collection_3.find({"_id":id_temp}))

    data['resnet_vector'] = data_1[0]['ir_vector']
    data['inception_vector'] = data_2[0]['ir_vector']
    data['vgg_vector'] = data_3[0]['ir_vector']
    # try:
    #     data['name_desc_vector'] = data_2[0]['name_vector']
    # except:
    #     print('hi')
    #     data['name_desc_vector'] = load_name_description_bert_model(data)
    #
    # try:
    #     data['name_vector'] = data_3[0]['name_vector']
    # except:
    #     print('hi')
    #     data['name_vector'] = load_name_bert_model(data)

    # data['vgg_vector'] = data_4[0]['ir_vector']
    # data.pop('resnet_vector')
    # data.pop('inception_vector')
    # data.pop('vgg_vector')
    return data


if __name__ == '__main__':
    print("starting your transfer")

    MONGODB_URL1 = 'mongodb://root:MTyvE6ikos87@mongodb-dev.greendeck.co:27017/admin'
    client_dev = pymongo.MongoClient(
       MONGODB_URL1,
       ssl=False)
    # MONGODB_URL2 = 'mongodb://admin:epxNOGMHaAiRRV5q@mongodb-prod.greendeck.co:27017/admin'
    # client_prod = pymongo.MongoClient(
    #     MONGODB_URL2,
    #     ssl=False)


    # source_db = client_prod['faissal']
    sink_db = client_dev['black_widow_development']
    source_db = client_dev['faissal_dev']
    source_collection = sink_db['hervis_combined_products_vec']
    source_collection_1 = source_db['hervis_mildnet_1024_2019_04_15']
    source_collection_2 = source_db['hervis_mildnet_vgg16_big_1024_2019_04_16']
    source_collection_3 = source_db['hervis_mildnet_vgg19_1024_2019_04_16']
    sink_collection = source_db['hervis_3ir_2nlp_mildnet_1024_2019_04_16']
    print('hi')
    # xxx = range(0,source_collection.count_documents({}),100)
    xxx = range(0,200396,50)
    print(str(xxx))
    for batch in tqdm(xxx):
        try:
            results = []
            int(batch)
            # pool = ThreadPool(10)
            list_view_objects = source_collection.find({}).sort("_id", 1).skip(batch).limit(50)
            print(batch)
            # results = pool.map(actually_move, list_view_objects)
            # pool.close()
            # pool.join()
            for post in list(list_view_objects):
                return_post = actually_move(post)
                results.append(return_post)
            try:
                sink_collection.insert_many(results)
                print("YOLO: "+str(sink_collection.count()))
            except:
                print("YOLO: "+str(sink_collection.count()))
                raise
        except Exception as e:
            raise
            print("failed: "+ str(e))
